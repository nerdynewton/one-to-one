class CreateMeetings < ActiveRecord::Migration
  def change
    create_table :meetings do |t|
      t.string :name
      t.datetime :date 
      t.string :recurring_status
      t.integer :recurring_id
      t.text :agenda
      t.string :status 
      t.integer :creator_id
      t.integer :guest_id
      t.timestamps null: false
    end
  end
end
