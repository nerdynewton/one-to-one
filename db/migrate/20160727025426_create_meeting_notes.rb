class CreateMeetingNotes < ActiveRecord::Migration
  def change
    create_table :meeting_notes do |t|
      t.references :meeting # meeting_id
      t.integer :creator_id #user_id
      t.text :content
      t.timestamps null: false
    end
  end
end
