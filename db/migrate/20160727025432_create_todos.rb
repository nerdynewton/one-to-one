class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.references :meeting 
      t.string :name
      t.boolean :is_completed 
      t.integer :creator_id 
      t.timestamps null: false
    end
  end
end
