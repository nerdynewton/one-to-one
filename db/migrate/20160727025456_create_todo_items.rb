class CreateTodoItems < ActiveRecord::Migration
  def change
    create_table :todo_items do |t|
      t.references :todo 
      t.integer :creator_id
      t.text :item
      t.boolean :is_completed
      t.boolean :is_deleted
      t.timestamps null: false
    end
  end
end
