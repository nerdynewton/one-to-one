class CreateProfileNotes < ActiveRecord::Migration
  def change
    create_table :profile_notes do |t|
      t.integer :writer_id
      t.integer :receiver_id
      t.text :content
      t.timestamps null: false
    end
  end
end
