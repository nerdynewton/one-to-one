class ApplicationMailer < ActionMailer::Base
  default from: 'newton@piktochart.com'
  layout 'mailer'
end
