class MeetingInvitationMailer < ApplicationMailer
	def invite_user(meeting,guest, creator)
		@meeting = meeting
		@guest = guest
		@creator = creator
		@meeting_invitation_url = 'http://google.com'
		email_with_name = %("#{@guest.name.split.map(&:capitalize)*' '}" <#{@guest.email}>)
  		mail(to: email_with_name, subject: 'Lighthouse : 1:1 Meeting Invitation from ' + @creator.name.split.map(&:capitalize)*' ')
	end
end
