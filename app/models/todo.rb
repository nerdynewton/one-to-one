class Todo < ActiveRecord::Base
	belongs_to :meeting
	belongs_to :creator, class_name: "User", foreign_key: :creator_id
	has_many :todo_items, :dependent => :destroy

	accepts_nested_attributes_for :todo_items
end
