class Meeting < ActiveRecord::Base
	
	belongs_to :creator, class_name: "User", foreign_key: :creator_id
	belongs_to :guest, class_name: "User", foreign_key: :guest_id
	has_many :meeting_notes, :dependent => :destroy
	has_many :todos, :dependent => :destroy

	accepts_nested_attributes_for :meeting_notes

	RECURRING_STATUS = ['Weekly','Bi-weekly', 'Monthly']

	validates_presence_of :name, :date, :guest_id

end

