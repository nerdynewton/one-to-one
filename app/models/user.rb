class User < ActiveRecord::Base

  #after_create :init_user_to_peer

  belongs_to :team
  has_many :profile_notes
  has_one :user_role
  has_one :role, through: :user_role 

  has_many :created_meetings, class_name: "Meeting", foreign_key: :creator_id
  has_many :invited_meetings, class_name: "Meeting", foreign_key: :guest_id

  has_many :meeting_notes
  has_many :todos
  has_many :todo_items
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
     :omniauthable, :omniauth_providers => [:google_oauth2]

  #paperclip
  has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/assets/img/profiles/avatar.jpg"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

  accepts_nested_attributes_for :user_role

  #validates_presence_of :name, :email, :password

  def self.find_for_google_oauth2(auth)
      data = auth.info
      user = User.where(:provider => auth.provider, :uid => auth.uid ).first

      if user
        user.access_token = auth.credentials.token
        user.refresh_token = auth.credentials.refresh_token
        user.save
        return user
      else
        registered_user = User.where(:email => auth.info.email).first
        if registered_user
          registered_user.access_token = auth.credentials.token
          registered_user.refresh_token = auth.credentials.refresh_token
          registered_user.save
          return registered_user
        else
          user = User.create(name: data["name"],
            provider:auth.provider,
            email: data["email"],
            uid: auth.uid ,
            password: Devise.friendly_token[0,20],
            access_token: auth.credentials.token,
            refresh_token: auth.credentials.refresh_token
          )
        end
     end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.google_data"] && session["devise.google_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def google
    @user = User.from_omniauth(request.env["omniauth.auth"])

    if @user.persisted?
      sign_in_and_redirect @user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => "Google") if is_navigational_format?
    else
      session["devise.google_data"] = request.env["omniauth.auth"]
      redirect_to new_user_registration_url
    end
  end

  def failure
    redirect_to root_path
  end

  def self.from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.password = Devise.friendly_token[0,20]
      end
  end 

  def admin?     
    if self.user_role.role_id == 4
      return true
    end
  end 

  def unity?     
    if self.user_role.role_id == 3
      return true
    end
  end 

  def team_leader?     
    if self.user_role.role_id == 2
      return true
    end
  end 

  def peer?     
    if self.user_role.role_id == 1
      return true
    end
  end 
end
