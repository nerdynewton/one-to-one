class ProfileNote < ActiveRecord::Base
	belongs_to :writer, class_name: "User", foreign_key: :writer_id
	belongs_to :receiver, class_name: "User", foreign_key: :receiver_id
end
