class TodoItemsController < ApplicationController
	def index 
		@todo_items = TodoItem.all 	
	end

	def new
		@todo_item = TodoItem.new
	end 

	def create
		@todo_item = TodoItem.new(todo_item_params)
		@todo_item.creator_id = current_user.id
		@todo_item.is_completed = false
		@todo_item.is_deleted = false
		@todo_item.save
		#redirect_to @todo_item
		render json: { message: 'item created', id: @todo_item.id }, status: 200
	end

	def show
		@todo_item = TodoItem.find(params[:id])
	end 

	def edit
		@todo_item = TodoItem.find(params[:id])
	end 

	def update
		is_completed = params["isCompleted"]
		@todo_item = TodoItem.find(params[:id])
  		if @todo_item.update(todo_item_params)
    		render json: { message: 'updated' }, status: 200
  		else
    		render 'edit'
  		end
	end

	def destroy
		@todo_item = TodoItem.find(params[:id])
		@todo_item.destroy
		redirect_to (:back)
	end

	private
	def todo_item_params
		params.require(:todo_item).permit(:item, :todo_id, :creator_id, :is_completed, :is_deleted)
	end
end
