class TeamsController < ApplicationController
	def index
		@teams = Team.all 	
	end

	def new
		@team = Team.new
	end

	def edit
		@team = Team.find(params[:id])	
	end 

	def show
		@team = Team.find(params[:id])
		@users = User.where(team_id: @team)
	end

	def create
		@team = Team.new(team_params)
		@team.save
		redirect_to @team
	end

	def update
		@team = Team.find(params[:id])
  		if @team.update(team_params)
    		redirect_to @team
  		else
    		render 'edit'
  		end
	end

	def destroy
		@team = Team.find(params[:id])
		@team.destroy
		redirect_to @team
	end

	private
	def team_params
		params.require(:team).permit(:name, :status)
	end

end
