class UsersController < ApplicationController

	def index 
		if current_user.team_leader?
			@users = User.all.where(:team_id => current_user.team_id)
		end
		if current_user.admin? || current_user.unity? 
			@users = User.all
		end
		if current_user.peer?
			@users= User.where(:id => current_user.id)
		end
	end

	def new
		if current_user.admin? || current_user.unity?
			@user = User.new
		end
	end

	def create_user
		if current_user.admin? || current_user.unity?
			@user = User.new(user_params)
			@user.email = @user.name + "@piktochart.com"
			@user.save
			redirect_to @user
		end
	end

	def edit
		@user = User.find(params[:id])
	end 

	def show
		@user = User.find(params[:id])
		@profile_notes = ProfileNote.where(receiver_id: @user.id, writer_id: current_user.id)
	end

	def update
		name = params["name"]
		email = params["email"]
		password = params["password"]
		avatar = params["avatar"]

		if !current_user.peer?
			@user = User.find(params[:id])
		else
			@user = current_user
		end
    	if @user.update_attributes(user_params)
    		if !params[:user_role_id].nil?
	    		user_role = @user.user_role
	    		user_role.role_id = params[:user_role_id]
	    		user_role.save
	    	end
      		flash[:notice] = "Update successfully"
      		redirect_to users_path
    	else
      		render 'edit'
      		flash[:notice] = "Update failed"
    	end
	end

	def destroy
		@user = User.find(params[:id])
		@user.destroy
		redirect_to @user
	end

	def all_meetings
		@user = User.find(params[:id])
		@meetings = @user.created_meetings.order("id ASC") + @user.invited_meetings.order("id ASC")
		@upcoming_meetings = @user.created_meetings.where('date >= ?', Date.current).order("id ASC") + @user.invited_meetings.where('date >= ?', Date.current).order("id ASC")
		@past_meetings = @user.created_meetings.where('date < ?', Date.current).order("id ASC") + @user.invited_meetings.where('date < ?', Date.current).order("id ASC")	
	end 

	private
	def user_params
		params.require(:user).permit(:avatar, :email, :password, :name, :status, :team_id, user_role_attributes: [:id, :role_id])
	end

end
