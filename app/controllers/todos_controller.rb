class TodosController < ApplicationController
	def index 
		@todos = Todo.where(meeting_id: params[:meeting_id])		
	end

	def new
		@todo = Todo.new
		@todo_item = @todo.todo_items.new
	end 

	def create
		creator_id = current_user.id
		is_deleted = false
		is_completed = false 
		@todo = Todo.new(todo_params)
		@todo.creator_id = current_user.id
		@todo.is_completed = is_completed
		@todo.todo_items.each do |todo_item|
			todo_item.creator_id = current_user.id
			todo_item.is_completed = is_completed
			todo_item.is_deleted = is_deleted
		end
		@todo.save
		redirect_to (meeting_path(:id => @todo.meeting_id))
	end

	def show
		@todo = Todo.find(params[:id])
	end 

	def edit
		@todo = Todo.find(params[:id])
		@todo_items = @todo.todo_items	
	end 

	def update
		@todo = Todo.find(params[:id])
  		if @todo.update(todo_params)
    		redirect_to (meeting_path(:id => @todo.meeting_id))
  		else
    		render 'edit'
  		end
	end

	def destroy
		@todo = Todo.find(params[:id])
		@todo.destroy
		redirect_to (meeting_path(:id => @todo.meeting_id))
	end

	private
	def todo_params
		params.require(:todo).permit(:meeting_id, :name, :is_completed, :creator_id, todo_items_attributes: [:todo_id, :id, :item, :is_completed, :is_deleted, :creator_id])
	end
end
