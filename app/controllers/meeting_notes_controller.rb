class MeetingNotesController < ApplicationController
	def index
		@meetingnotes = MeetingNote.where(:meeting_id => params[:meeting_id], :creator_id => current_user.id)
	end

	def new
		@meeting_id = params[:meeting_id]
		@creator_id = current_user.id
		@meetingnote = MeetingNote.new
	end

	def create
		@meetingnote = MeetingNote.new(meetingnote_params)
		@meetingnote.save
		redirect_to (meeting_path(:id => @meetingnote.meeting_id))
	end

	def show 
		@meetingnote = MeetingNote.find(params[:meeting_id]).where(:creator_id => current_user.id)
		puts @meetingnote
	end

	def edit
		@meetingnote = MeetingNote.find(params[:id])
	end

	def update
		@meetingnote = MeetingNote.find(params[:id])
 
  		if @meetingnote.update(meetingnote_params)
    		render json: { message: 'updated' }, status: 200
  		else
    		render 'edit'
  		end
	end

	def destroy
		@meetingnote = MeetingNote.find(params[:id])
		@meetingnote.destroy
		redirect_to (:back)
	end

	private
	def meetingnote_params
		params.require(:meeting_note).permit(:content, :meeting_id, :creator_id )
	end
end
