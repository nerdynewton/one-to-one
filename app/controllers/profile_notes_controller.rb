class ProfileNotesController < ApplicationController
	def index
		if current_user.unity? || current_user.team_leader?
	  		@receiver = User.find(params[:user_id])
	  		@writer = current_user.id 
	  		@profile_notes = ProfileNote.where(:writer_id => @writer, :receiver_id => @receiver)
	  	else 
	  		redirect_to(:back)
	  	end
	end

  	def new
  		if current_user.unity? || current_user.team_leader?
  			flash.now[:notice] = 'Message sent!'
			@profile_note = ProfileNote.new
		else
			redirect_to(:back)
		end
	end 

	def create	
		content = params["content"]
		writer_id = current_user.id
		receiver_id = params["receiver_id"]
		

		@profile_note = ProfileNote.create!(content: content, writer_id: writer_id, receiver_id: receiver_id)
		@profile_note.save
		redirect_to @profile_note	
	end

	def show
		@profile_note = ProfileNote.find(params[:id])
	end 

	def edit
		@profile_note = ProfileNote.find(params[:id])
		
	end 

	def update
		@profile_note = ProfileNote.find(params[:id])
 
  		if @profile_note.update(profile_note_params)
    		redirect_to @profile_note
  		else
    		render 'edit'
  		end
	end

	def destroy
		@profile_note = ProfileNote.find(params[:id])
		@profile_note.destroy
		redirect_to (:back)
	end

	private
	def profile_note_params
		params.require(:profile_note).permit(:content, :writer_id, :receiver_id)
	end
end
