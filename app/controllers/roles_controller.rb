class RolesController < ApplicationController
	def index
		if current_user.admin? 
			@roles = Role.all 
		else
			redirect_to(:back)			
		end 	
	end

	def new
		if current_user.admin?
			@role = Role.new
		else
			redirect_to(:back)
		end
	end 

	def create
		@role = Role.new(role_params)
		@role.save
		redirect_to @role
	end

	def show
		if current_user.admin?
			@role = Role.find(params[:id])
		else
			redirect_to(:back)
		end
	end 

	def edit
		if current_user.admin?
			@role = Role.find(params[:id])
		else
			#redirect_to(:back)
		end	
	end 

	def update
		@role = Role.find(params[:id])
 
  		if @role.update(role_params)
    		redirect_to @role
  		else
    		render 'edit'
  		end
	end

	def destroy
		@role = Role.find(params[:id])
		@role.destroy
		redirect_to @role
	end

	private
	def role_params
		params.require(:role).permit(:name)
	end
end
