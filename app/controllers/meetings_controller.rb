class MeetingsController < ApplicationController
	before_action :get_meeting, only: [:update, :destroy, :show]
	respond_to :js

	def index
		@meeting = Meeting.new
		@meeting_note = @meeting.meeting_notes.new
		@meetings = current_user.created_meetings.order("id ASC") + current_user.invited_meetings.order("id ASC")
		@upcoming_meetings = current_user.created_meetings.where('date >= ?', Date.current).order("id ASC") + current_user.invited_meetings.where('date >= ?', Date.current).order("id ASC")
		@past_meetings = current_user.created_meetings.where('date < ?', Date.current).order("id ASC") + current_user.invited_meetings.where('date < ?', Date.current).order("id ASC")		
	end

	def new
		@meeting = Meeting.new
		@meeting_note = @meeting.meeting_notes.new
	end

	def create
		@guest_id = params["guest_id"]
		@date = params["date"]
		@name = params["name"]
		@content = params["content"]
		@agenda = params["agenda"]
		@meeting = Meeting.create!(agenda: @agenda, creator_id: current_user.id, guest_id: @guest_id, date: @date, name: @name)
		@meeting_notes = MeetingNote.create!(content: @content, creator_id: current_user.id, meeting_id: @meeting.id)
		if !@meeting.meeting_notes.first.content.nil?
			@meeting.meeting_notes.first.creator_id = current_user.id
		end
		@meeting.meeting_notes.first.creator_id = current_user.id
		if !@meeting.save
			flash[:alert] = @meeting.errors.full_messages
		end
		if @meeting.meeting_notes.first.content == ""
			MeetingNote.last.delete #remove last meeting note as the value is blank
		end
		notify_guest(@meeting)
		render json: { message: 'meeting created', date: @meeting.date, guest: User.find(@meeting.guest_id).name, topic: @meeting.name }, status: 200
	end

	def get_guest_list
		guest_list = User.where.not(:id => current_user.id)
	end

	def check_meeting_status(meeting)
		@meeting = meeting
		if @meeting.status == 'accepted'
			set_status_to_scheduled(@meeting)
			google_calendar_create_host(@meeting)
			google_calendar_create_guest(@meeting)
		end
	end

	def google_calendar_create_host(meeting)
		@meeting = meeting 
		event = {
                'summary' => @meeting.name,
                'location' => 'Piktochart HQ at Suntech',
                'start' => {
                'dateTime' => @meeting.date.to_datetime},  
               	'end' => {         
		          'dateTime' => @meeting.date.to_datetime},                 
		          'attendees' => User.find(@meeting.guest_id).name          
                 }       
		token = User.find(@meeting.creator_id).access_token
      	client = Google::APIClient.new         
      	client.authorization.access_token = token
        service = client.discovered_api('calendar', 'v3')
        result = client.execute(:api_method => service.events.insert, :parameters => {'calendarId' => 'primary'},:body => JSON.dump(event),:headers => {'Content-Type' => 'application/json'})
	end

	def google_calendar_create_guest(meeting)
		@meeting = meeting 
		event = {
                'summary' => @meeting.name,
                'location' => 'Piktochart HQ at Suntech',
                'start' => {
                'dateTime' => @meeting.date.to_datetime},  
               	'end' => {         
		          'dateTime' => @meeting.date.to_datetime},                 
		          'attendees' => User.find(@meeting.guest_id).name          
                 }       
		token = current_user.access_token
      	client = Google::APIClient.new         
      	client.authorization.access_token = token
        service = client.discovered_api('calendar', 'v3')
        result = client.execute(:api_method => service.events.insert, :parameters => {'calendarId' => 'primary'},:body => JSON.dump(event),:headers => {'Content-Type' => 'application/json'})
	end

	def set_status_to_scheduled(meeting)
		@meeting = meeting
		@meeting.update(:status => 'scheduled')
	end

	def notify_guest(meeting)
		@meeting = meeting
		MeetingInvitationMailer.invite_user(@meeting,User.find(@meeting.guest_id),User.find(@meeting.creator_id)).deliver_now
	end

	def show 	
		render json: { message: 'meeting info', date: @meeting.date, guest: User.find(@meeting.guest_id).name, topic: @meeting.name, agenda: @meeting.agenda }, status: 200
		check_meeting_status(@meeting)
	end

	def edit
		@meeting = Meeting.find(params[:id])

	end

	def update
  		if @meeting.update(meeting_params)
  			check_meeting_status(@meeting)
    		render json: {message: "scheduled"}, status: 200
  		else
    		render 'edit'
  		end
	end

	def destroy
		@meeting.destroy
		redirect_to @meeting
	end

	private
	def get_meeting
		@meeting = Meeting.find(params[:id])
	end
	def meeting_params
		params.require(:meeting).permit(:status, :name, :date, :agenda, :recurring_status, :creator_id, :guest_id, meeting_notes_attributes: [:id, :creator_id, :meeting_id, :content])
	end

	

end



