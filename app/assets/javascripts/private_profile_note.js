$('document').ready(function(){
	$('#newProfileNoteSlideUp').click(function(event){
		$('#newProfileNote').modal('show');
	});

	$('#createProfileNote').click(function(event){
		event.preventDefault();
		var url = '/profile_notes/'; 
		var profileNote = $('#profile-note-data').val(); 
		var receiverID = $('#receiver_id').val(); 
		var data = JSON.stringify({
			'content' : profileNote,
			'receiver_id' : receiverID
		});
		var newProfileNoteRequest = $.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				$('#newProfileNote').modal('hide');
				window.location.href = "/users/"+receiverID;
			}
		})

	});
});