$('document').ready(function(){

	let meetingID;

	$('.newMeetingNoteSlideUp').click(function(event){	
		$('#newMeetingNote').modal('show');
		meetingID = $(this).parent().find('#upcoming_meeting_id').val();
	});

	$('#createMeetingNote').click(function(event){
		event.preventDefault();
		var content = $('#meeting-note-data').val(); 
		var creatorID = $('#creator_id').val();
		var url = '/meeting_notes';
		var data = JSON.stringify({
			'content' : content,
			'creator_id' : creatorID,
			'meeting_id' : meetingID
		});

		var newMeetingNoteRequest = $.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				$('#newMeetingNote').modal('hide')
				window.location.href = "/meetings";
			}
		})
	});

});