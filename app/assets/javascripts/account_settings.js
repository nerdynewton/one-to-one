$('document').ready(function(){
	$('#accountSettingsSlideUp').click(function(event){
		event.preventDefault();
		$('#accountSettings').modal('show');
	});

	$('#saveAccountSettings').click(function(event){
		event.preventDefault();
		
		var email = $('#email-data').val(); 
		var name = $('#name-data').val(); 
		var password = $('#new-password-data').val(); 
		var avatar = $('#avatar-data').val();
		var user_id = $('#user_id').val();
		var url = '/users/'+user_id; 
		var data = JSON.stringify({
			'email' : email,
			'name' : name,
			'password' : password,
			'avatar' : avatar
		});
		var newProfileNoteRequest = $.ajax({
			type: 'PUT',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				$('#accountSettings').modal('hide');
				window.location.href = "/users/"+user_id;
			}
		})

	});

});