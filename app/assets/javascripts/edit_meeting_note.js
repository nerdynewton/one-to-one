$('document').ready(function(){
	$('.editMeetingNoteSlideUp').click(function(event){	
		$('#editMeetingNote').modal('show');
		var meetingNoteContent = $(this).parents('.meeting-notes').children('#meetingNoteContent').text(); //$('this.parent() > #meetingNoteContent').text();
		$('#edit-meeting-note').val(meetingNoteContent) ;
	});

	$('#updateMeetingNote').click(function(event){
		event.preventDefault();
		var content = $('#edit-meeting-note').val(); 
		var meetingID = $('#meeting_id').val();
		var creatorID = $('#creator_id').val();
		var id = $('#meeting_note_id').val();
		var url = '/meeting_notes/'+id; 
		var data = JSON.stringify({
			'content' : content,
			'meeting_id' : meetingID,
			'creator_id' : creatorID
		});
		var updateMeetingNoteRequest = $.ajax({
			type: 'PATCH',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				$('#editMeetingNote').modal('hide');
				window.location.href = "/meetings";
			}
		})

	});
});