$('document').ready(function(){

	let meetingID;

	$('.newTodoSlideUp').click(function(event){	
		$('#newTodo').modal('show');
		meetingID = $(this).parent().find('#past_meeting_id').val();

	});	

	$('#createTodo').click(function(event){
		event.preventDefault();
		var name = $('#new-todo-name').val();
		var url = '/todos';
		var data = JSON.stringify({
			'name' : name,
			'meeting_id' : meetingID
		});

		var newTodoRequest = $.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				alert('success');
				$('#newMTodo').modal('hide');
				window.location.href = "/meetings";
			}
		})
	});
});