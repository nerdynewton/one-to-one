$('document').ready(function(){
	$('.addTodo').keypress(function(event){
		if(event.keyCode == '13'){
			var item = $(this).val();
			var todo_id = $(this).parent().find('#getTodoID').val();
			var url = '/todo_items/';
			var data = JSON.stringify({	
				'item' : item,
				'todo_id' : todo_id
			});
			var addTodoRequest = $.ajax({
				type: 'POST',
				url: url,
				beforeSend: function(x){
					x.setRequestHeader('Content-Type', 'application/json');
				},
				data: data,
				success: function( data ){
					var item_id = data.id;
					stickToTodoDiv(item_id);
				}
			})
			function stickToTodoDiv(item_id){
				$('.todoList').append('<div class="checkbox check-success" id="todoItemChecker"><input type="checkbox" id="' + item_id + '" > <label for="' +item_id +'">' + item + '</label> </div>');
				$('.addTodo').val("");
			}
			
		}
	});
});