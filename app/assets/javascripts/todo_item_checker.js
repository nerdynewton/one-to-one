$('document').ready(function(){
	$('#todoItemChecker > input').on("click", function(event){
		var todoItemID = this.id;
		var url = '/todo_items/'+todoItemID;
		if (this.checked){
			var isCompleted = true;
		}else{
			var isCompleted = false;
		}
		var data = JSON.stringify({
			
			'todo_item' : { 'is_completed' : isCompleted }
		});

		var todoItemCompletedRequest = $.ajax({
			type: 'PUT',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(){
				console.log('todo Item updated')
			}
		})
	});
});