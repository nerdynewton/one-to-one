$('document').ready(function(){
	$('#teamMembersInfoSlideUp').click(function(event){
		event.preventDefault();
		$('#teamMembersInfo').modal('show');
	});

	$('#teamMember > tr').click(function(event){
		user_id = this.id;
		window.location.href = "/users/" + user_id;
	});
});