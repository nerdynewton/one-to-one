$('document').ready(function(){
	$('.progress').hide();
	$('#createMeeting').click(function(event){		
		$('.progress').show();
		event.preventDefault();
		var url = '/meetings/'; 
		var guestID = $('#User_guest_id').val(); 
		var privateNote = $('#private-note-data').val(); 
		var date = $('#date-data').val(); 
		var topic = $('#topic-data').val(); 
		var agenda = $('#agenda-data').val(); 	
		var data = JSON.stringify({
			'guest_id' : guestID,
			'content' : privateNote,
			'date' : date,
			'name' : topic,
			'agenda' : agenda
		});
		var newMeetingRequest = $.ajax({
			type: 'POST',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(data){
				$('#modalSlideUp').modal('hide');
				$('#confirmation-name').append('<i class="fa fa-user" aria-hidden="true"></i>     '+data.guest);
				$('#confirmation-date').append('<i class="fa fa-calendar" aria-hidden="true"></i>     '+data.date);
				$('#confirmation-topic').append('<i class="fa fa-commenting-o" aria-hidden="true"></i>     '+data.topic);
				$('#meetingConfirmation').modal('show');
			}
		})
	});

	$('#dismissConfirmation').click(function(event){
		window.location.href = "/meetings";
	});

	$('#datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD HH:mm'
    });
});