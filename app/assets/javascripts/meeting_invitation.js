$('document').ready(function(){

	let meetingid;

	$('.meetingInvitationNotification').click(function(event){
		event.preventDefault();

		$('#meeting-guest').empty();
		$('#meeting-date').empty();
		$('#meeting-topic').empty();
		$('#meeting-agenda').empty();

		$('#meetingInvitation').modal('show');
		meetingid = $(this)[0].dataset.meetingid;
		var url = '/meetings/'+meetingid; 

		var getMeetingInfoRequest = $.ajax({
			type: 'GET',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			success: function(data){
				$('#meeting-guest').append('<i class="fa fa-user" aria-hidden="true"></i>     '+data.guest);
				$('#meeting-date').append('<i class="fa fa-calendar" aria-hidden="true"></i>     '+data.date);
				$('#meeting-topic').append('<i class="fa fa-commenting-o" aria-hidden="true"></i>     '+data.topic);
				$('#meeting-agenda').append('<i class="fa fa-commenting-o" aria-hidden="true"></i>     '+data.agenda);
			}
		})
	});

	$('#confirmMeetingBtn').click(function(event){
		var status = "accepted";
		var url = '/meetings/'+meetingid;
		var data = JSON.stringify({
			'status' : status
		});

		var updateMeetingStatusRequest = $.ajax({
			type: 'PATCH',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(data){
				alert('success');
				$('.meetingInvitationNotification').modal('hide');
				window.location.href = "/meetings";
			}
		})
	});

	$('#cancelMeetingBtn').click(function(event){
		var status = "rejected";
		var url = '/meetings/'+meetingid;
		var data = JSON.stringify({
			'status' : status
		});

		var updateMeetingStatusRequest = $.ajax({
			type: 'PATCH',
			url: url,
			beforeSend: function(x){
				x.setRequestHeader('Content-Type', 'application/json');
			},
			data: data,
			success: function(data){
				$('.meetingInvitationNotification').modal('hide');
				window.location.href = "/meetings";
			}
		})
	});
	


});