Rails.application.routes.draw do

  get 'profile_notes/index'

   devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

   resources :teams, :roles, :meetings, :meeting_notes, :todos, :todo_items, :profile_notes

   resources :users, except: :create do
    resources :profile_notes, only: [:new, :edit, :show, :index]
    collection do   
     post :create_user
    end
    member do
      get :all_meetings
    end
    
   end

   resources :meetings do
    resources :meeting_notes, only: [:new, :edit, :show, :index]
    resources :todos, only: [:new, :edit, :show, :index]
   end
   # get '/users/:id/all_meetings', to: 'meetings#all_meetings'

   get '/agenda' => 'meetings#agenda'

   get '/note' => 'meetings#note', as: :show_all_notes

   root 'meetings#index'

end
